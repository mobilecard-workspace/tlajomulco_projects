<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Respuesta Pago Tlajomulco</title>
<style type="text/css">
#contenedor {
	width: 275px;
	margin: 0 auto;
}

html {
	font-family: arial;
	font-size: 12px;
	font-weight: bold;
	color: #F17E44;
	background-color: #FFFFFF;
}

td {
	font-family: arial;
	font-size: 12px;
}

.title {
	font-family: arial;
	font-size: 12px;
}

.title2 {
	font-family: arial;
	font-size: 12px;
}

input,select {
	width: 240px;
	height: 35px;
	font-family: arial;
	font-size: 14px;
}

select.mes {
	width: 120px;
}

.anio {
	width: 120px;
}

p.info {
	margin-top: 5px;
	margin-bottom: 5px;
}
</style>
</head>
<body>

	<div id="contenedor">
		<p style="text-align: center;">Portal 3D Secure Tlajomulco</p>
		<p style="text-align: center;">Transaccion exitosa</p>
		<form name="form1" action="#">
			<input type="hidden" name="autorizacion" value="${prosa.emAuth}"/>
 			<input type="hidden" name="referencia1" value="${prosa.emRefNum}"/>
 			<input type="hidden" name="referencia2" value="${prosa.emOrderID}"/>
 			<input type="hidden" id="imprimir" name="imprimir" value=""/>

			<table cellpadding="1" cellspacing="1">
				<tr>
					<td colspan="2" align="center">
						<h1>Transaccion exitosa</h1>
					</td>
				</tr>
				<tr>
					<td colspan="2">Su pago fue procesado correctamente.</td>
				</tr>
				<tr>
					<td></br></td>
					<td></td>
				</tr>
				<tr>
					<td>Servicio pagado:</td>
					<td>${prosa.tipoServicio}</td>
				</tr>
				<tr>
					<td>Cuenta:</td>
					<td>${prosa.cuenta}</td>
				</tr>
				<tr>
					<td>Folio Addcel:</td>
					<td>${prosa.emOrderID}</td>
				</tr>
							
				<tr>
					<td>Autorización:</td>
					<td>${prosa.emAuth}</td>
				</tr>
				<tr>
					<td>Comisión:</td>
					<td>${prosa.comision}</td>
				</tr>
				<tr>
					<td>Total:</td>
					<td>${prosa.emTotal}</td>
				</tr>				
			</table>
		</form>
	</div>
</body>
</html>