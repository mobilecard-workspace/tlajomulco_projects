package com.addcel.tlajomulco.model.vo.request;

import java.io.Serializable;

public class RequestCatRoles implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String id_aplicacion;
	private String id_proveedor;

	public String getId_aplicacion() {
		return id_aplicacion;
	}

	public void setId_aplicacion(String id_aplicacion) {
		this.id_aplicacion = id_aplicacion;
	}

	public String getId_proveedor() {
		return id_proveedor;
	}

	public void setId_proveedor(String id_proveedor) {
		this.id_proveedor = id_proveedor;
	}
}