package com.addcel.tlajomulco.model.mapper;

import java.util.List;


import org.apache.ibatis.annotations.Param;
import org.springframework.dao.DuplicateKeyException;

import com.addcel.tlajomulco.model.vo.ComisionVO;
import com.addcel.tlajomulco.model.vo.RoleVO;
import com.addcel.tlajomulco.model.vo.ServicioVO;
import com.addcel.tlajomulco.model.vo.TiendaVO;
import com.addcel.tlajomulco.model.vo.UserVO;
import com.addcel.tlajomulco.model.vo.response.DetalleVO;
import com.addcel.tlajomulco.model.vo.response.LoginRespuesta;
import com.addcel.tlajomulco.model.vo.TlajomulcoDetalleVO;

public interface TlajomulcoMapper {
	int addUser(UserVO usuario);

	LoginRespuesta loginUsuario(@Param(value = "idUser") int idUser);

	List<TiendaVO> listaTiendas(@Param(value = "proveedor") String proveedor);

	int insertaModulosUsuario(@Param(value = "idUser") int idUser,
			@Param("idRol") int idRol) throws DuplicateKeyException;

	int updateUser(UserVO usuario);

	List<UserVO> findUser(@Param(value = "nombre") String nombre,
			@Param(value = "paterno") String paterno,
			@Param(value = "materno") String materno,
			@Param(value = "login") String login,
			@Param(value = "idSupervisor") String idSupervisor,
			@Param(value = "idTienda") String idTienda
			);
	
	List<UserVO> getVendor(@Param(value = "idSupervisor") String nombre,
			@Param(value = "idUser") String paterno,
			@Param(value = "idTienda") String materno,
			@Param(value = "login") String login);

	List<RoleVO> getCatRoles(@Param(value = "aplicacion") String aplicacion,
			@Param(value = "proveedor") String proveedor);

	List<ServicioVO>getListServicios();
	
	int updateServicioStatus(@Param(value = "tipo_servicio") int tipo_servicio,
							 @Param(value = "status") int status);
	
	String getPassword(@Param(value = "idUser")int idUser);
	
	String getFechaActual();

	int difFechaMin(String fechaToken);

	int getIdUser(int idUser);

	List<DetalleVO> getDetalle(@Param(value = "supervisor") String supervisor,
			@Param(value = "user") String user,
			@Param(value = "tienda") String tienda,
			@Param(value = "fi") String fechaInicial,
			@Param(value = "ff") String fechaFinal,
			@Param(value = "idRol") String idRol,
			@Param(value = "idBitacora") String idBitacora);

	UserVO userIntentosLogin(@Param(value = "login") String login);

	int updateIntentosStatus(@Param(value = "idUser") int idUser,
			@Param(value = "valor") int valor,
			@Param(value = "update") String update);
	
	ComisionVO getComisiones(@Param(value = "idProveedor")int idProveedor);
	
	String getAfiliacion(@Param(value = "idProveedor")int idProveedor);
	
	String getParametro(@Param(value = "clave")String clave);
	
	List<TlajomulcoDetalleVO> getAdeudos(@Param(value = "cuenta") String cuenta, @Param(value = "referencia") String referencia, @Param(value = "tipo_servicio")String tipo_servicio);
	
	String getCuentaID(TlajomulcoDetalleVO td);
}
