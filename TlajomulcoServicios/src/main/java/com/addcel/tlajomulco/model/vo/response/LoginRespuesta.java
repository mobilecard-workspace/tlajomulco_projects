package com.addcel.tlajomulco.model.vo.response;

import java.util.Date;
import java.util.List;

import com.addcel.tlajomulco.model.vo.AbstractResponse;
import com.addcel.tlajomulco.model.vo.ComisionVO;
import com.addcel.tlajomulco.model.vo.ModuleVO;
import com.addcel.tlajomulco.model.vo.TiendaVO;

public class LoginRespuesta extends AbstractResponse{	
	private TiendaVO tienda;
	private ComisionVO comisiones;
	private List<ModuleVO> modulos;
	private int idUser;
	private Date lastLogin;
	private int loginCount;
	private int idStatus;
	private int idRol;
	
	public int getIdRol() {
		return idRol;
	}
	public void setIdRol(int idRol) {
		this.idRol = idRol;
	}
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public Date getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}
	public int getLoginCount() {
		return loginCount;
	}
	public void setLoginCount(int loginCount) {
		this.loginCount = loginCount;
	}
	public int getIdStatus() {
		return idStatus;
	}
	public void setIdStatus(int idStatus) {
		this.idStatus = idStatus;
	}
	public TiendaVO getTienda() {
		return tienda;
	}
	public void setTienda(TiendaVO tienda) {
		this.tienda = tienda;
	}	
	public ComisionVO getComisiones() {
		return comisiones;
	}
	public void setComisiones(ComisionVO comisiones) {
		this.comisiones = comisiones;
	}
	public List<ModuleVO> getModulos() {
		return modulos;
	}
	public void setModulos(List<ModuleVO> modulos) {
		this.modulos = modulos;
	}	
}
