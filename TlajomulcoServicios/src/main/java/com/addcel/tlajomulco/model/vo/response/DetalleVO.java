package com.addcel.tlajomulco.model.vo.response;

import com.addcel.tlajomulco.model.vo.TlajomulcoDetalleVO;

public class DetalleVO extends TlajomulcoDetalleVO {

	private String referencia;
	private String nombre;
	private String last4d;

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getLast4d() {
		return last4d;
	}

	public void setLast4d(String last4d) {
		this.last4d = last4d;
	}
}