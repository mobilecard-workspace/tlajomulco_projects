package com.addcel.tlajomulco.model.vo.request;

public class UpdateServicioVO {
	private int tipo_servicio;
	private int status;
	private int idUser;
	private String password;
	public int getTipo_servicio() {
		return tipo_servicio;
	}
	public void setTipo_servicio(int tipo_servicio) {
		this.tipo_servicio = tipo_servicio;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
