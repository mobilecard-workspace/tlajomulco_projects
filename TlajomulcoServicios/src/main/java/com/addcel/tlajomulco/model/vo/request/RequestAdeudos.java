package com.addcel.tlajomulco.model.vo.request;

public class RequestAdeudos {
	private String cuenta;
	private String referencia;
	private int tipo_servicio;
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public int getTipo_servicio() {
		return tipo_servicio;
	}
	public void setTipo_servicio(int tipo_servicio) {
		this.tipo_servicio = tipo_servicio;
	}
}
