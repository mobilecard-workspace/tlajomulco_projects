package com.addcel.tlajomulco.model.vo.response;

import com.addcel.tlajomulco.model.vo.TlajomulcoDetalleVO;
import java.util.List;

public class ResponseAdeudo {

	private List<TlajomulcoDetalleVO> adeudos;

	public List<TlajomulcoDetalleVO> getAdeudos() {
		return adeudos;
	}

	public void setAdeudos(List<TlajomulcoDetalleVO> adeudos) {
		this.adeudos = adeudos;
	}
}
