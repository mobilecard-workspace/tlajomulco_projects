package com.addcel.tlajomulco.model.vo;

import java.math.BigDecimal;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TlajomulcoDetalleVO extends TlajomulcoVO {
	private Long id_bitacora;
	private String id_usuario;
	private String fecha;
	private int status;
	private String email;
	private BigDecimal comision;
	private BigDecimal total_pago;
	private String nombre_tarjeta;
	
	public Long getId_bitacora() {
		return id_bitacora;
	}
	public void setId_bitacora(Long id_bitacora) {
		this.id_bitacora = id_bitacora;
	}
	public String getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(String id_usuario) {
		this.id_usuario = id_usuario;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public BigDecimal getComision() {
		return comision;
	}
	public void setComision(BigDecimal comision) {
		this.comision = comision;
	}
	public BigDecimal getTotal_pago() {
		return total_pago;
	}
	public void setTotal_pago(BigDecimal total_pago) {
		this.total_pago = total_pago;
	}
	public String getNombre_tarjeta() {
		return nombre_tarjeta;
	}
	public void setNombre_tarjeta(String nombre_tarjeta) {
		this.nombre_tarjeta = nombre_tarjeta;
	}	
}
