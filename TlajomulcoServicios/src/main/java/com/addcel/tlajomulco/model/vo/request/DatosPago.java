package com.addcel.tlajomulco.model.vo.request;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import com.addcel.tlajomulco.model.vo.TlajomulcoDetalleVO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DatosPago extends TlajomulcoDetalleVO{

	private static final long serialVersionUID = 217828031301781865L;
	
	private String token;
	private String imei;
	private String tipo;
	private String software;
	private String modelo;
	private String wkey;
	private String cx;
	private String cy;

	
	private String referencia;
	private String noAutorizacion;
	private String mensajeError;
	private int status;
	private String fecha;
	private String descRechazo;
	
	private String msg;
	private String transaccion;
	

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getSoftware() {
		return software;
	}

	public void setSoftware(String software) {
		this.software = software;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getWkey() {
		return wkey;
	}

	public void setWkey(String wkey) {
		this.wkey = wkey;
	}

	public String getCx() {
		return cx;
	}

	public void setCx(String cx) {
		this.cx = cx;
	}

	public String getCy() {
		return cy;
	}

	public void setCy(String cy) {
		this.cy = cy;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getNoAutorizacion() {
		return noAutorizacion;
	}

	public void setNoAutorizacion(String noAutorizacion) {
		this.noAutorizacion = noAutorizacion;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getDescRechazo() {
		return descRechazo;
	}

	public void setDescRechazo(String descRechazo) {
		this.descRechazo = descRechazo;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getTransaccion() {
		return transaccion;
	}

	public void setTransaccion(String transaccion) {
		this.transaccion = transaccion;
	}
}
