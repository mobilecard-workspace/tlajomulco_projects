package com.addcel.tlajomulco.model.vo;

public class ServicioVO {
	private int tipo_servicio;
	private String descripcion;
	private int status;
	public int getTipo_servicio() {
		return tipo_servicio;
	}
	public void setTipo_servicio(int tipo_servicio) {
		this.tipo_servicio = tipo_servicio;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
}
