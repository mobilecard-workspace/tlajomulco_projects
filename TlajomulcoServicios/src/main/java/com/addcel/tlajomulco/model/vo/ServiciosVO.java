package com.addcel.tlajomulco.model.vo;

import java.util.List;

import com.addcel.tlajomulco.model.vo.request.UpdateServicioVO;

public class ServiciosVO {
	private List<UpdateServicioVO>servicios;

	public List<UpdateServicioVO> getServicios() {
		return servicios;
	}

	public void setServicios(List<UpdateServicioVO> servicios) {
		this.servicios = servicios;
	}
}
