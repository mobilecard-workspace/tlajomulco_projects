package com.addcel.tlajomulco.model.vo;

import java.io.Serializable;

public class DuttyFreeDetalleVO implements Serializable{
	
	private static final long serialVersionUID = -3216445473867726951L;
	
	private int idDetalle;
	private long idBitacora;
	private String idUsuario;
	private String fecha;
	private int status;
	private String descProducto;
	private Double total;
	private String moneda;
	private Double tipoCambio;
	private String nacionalidad;
	private String pasaporte;
	private String nombre_completo;
	private String fecha_nacimiento;
	private String numero_vuelo;
	private String email;
	private Double comision;
	
	public DuttyFreeDetalleVO() {}
	
	public DuttyFreeDetalleVO(long idBitacora, String idUsuario,
			String descProducto, Double total,
			String moneda, Double tipoCambio,
			String nacionalidad, String pasaporte,
			String nombre_completo, String fecha_nacimiento,
			String numero_vuelo, String email, Double comision) {
		super();		
		this.idBitacora = idBitacora;
		this.idUsuario = idUsuario;				
		this.descProducto = descProducto;
		this.total = total;
		this.moneda = moneda;
		this.tipoCambio = tipoCambio;
		this.nacionalidad = nacionalidad;
		this.pasaporte = pasaporte;
		this.nombre_completo = nombre_completo;
		this.fecha_nacimiento = fecha_nacimiento;
		this.numero_vuelo = numero_vuelo;
		this.email = email;
		this.comision = comision;
		
	}
	public int getIdDetalle() {
		return idDetalle;
	}
	public void setIdDetalle(int idDetalle) {
		this.idDetalle = idDetalle;
	}
	public long getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(long idBitacora) {
		this.idBitacora = idBitacora;
	}
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getDescProducto() {
		return descProducto;
	}
	public void setDescProducto(String descProducto) {
		this.descProducto = descProducto;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public Double getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(Double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

	public String getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	public String getPasaporte() {
		return pasaporte;
	}

	public void setPasaporte(String pasaporte) {
		this.pasaporte = pasaporte;
	}

	public String getNombre_completo() {
		return nombre_completo;
	}

	public void setNombre_completo(String nombre_completo) {
		this.nombre_completo = nombre_completo;
	}

	public String getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public void setFecha_nacimiento(String fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}

	public String getNumero_vuelo() {
		return numero_vuelo;
	}

	public void setNumero_vuelo(String numero_vuelo) {
		this.numero_vuelo = numero_vuelo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Double getComision() {
		return comision;
	}

	public void setComision(Double comision) {
		this.comision = comision;
	}
}
