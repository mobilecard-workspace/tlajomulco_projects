package com.addcel.tlajomulco.model.vo;

public class ModuleRoleVO {
	private int idRole;
	private int idModuel;
	public int getIdRole() {
		return idRole;
	}
	public void setIdRole(int idRole) {
		this.idRole = idRole;
	}
	public int getIdModuel() {
		return idModuel;
	}
	public void setIdModuel(int idModuel) {
		this.idModuel = idModuel;
	}
}
