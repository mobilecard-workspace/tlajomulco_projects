package com.addcel.tlajomulco.model.mapper;

import org.apache.ibatis.annotations.Param;


import com.addcel.tlajomulco.model.vo.TlajomulcoDetalleVO;
import com.addcel.tlajomulco.model.vo.pagos.TBitacoraProsaVO;
import com.addcel.tlajomulco.model.vo.pagos.TBitacoraVO;

public interface BitacorasMapper {
	int addBitacoraProsa(TBitacoraProsaVO b);
	int addBitacora(TBitacoraVO b);
	int updateBitacoraProsa(TBitacoraProsaVO b);
	int updateBitacora(TBitacoraVO b);
	int addTDetalle(TlajomulcoDetalleVO td);
	int updateTDetalle(@Param(value="idBitacora")long idBitacora,@Param(value="status")int status, @Param(value="cc_name")String cc_name);
	Integer validaAddFDetalle(TlajomulcoDetalleVO td); 
}
