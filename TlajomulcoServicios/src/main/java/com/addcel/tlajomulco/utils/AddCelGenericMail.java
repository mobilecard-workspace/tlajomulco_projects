package com.addcel.tlajomulco.utils;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.tlajomulco.model.vo.response.DetalleVO;
import com.addcel.tlajomulco.utils.UtilsService;

public class AddCelGenericMail {
	private static final Logger logger = LoggerFactory.getLogger(AddCelGenericMail.class);
	
	private static final String URL_MAIL = "http://50.57.192.214:8080/MailSenderAddcel/envia-recibo-tlajomulco/";
	
	public static boolean generatedMail(DetalleVO detalleVO, UtilsService utilsService){
		logger.info("Genera objeto para envio de mail: " + detalleVO.getEmail());
		boolean res = false;
		try{
			String jsonObject = utilsService.objectToJson(detalleVO);
			logger.info("json correo: " + jsonObject);
			StringBuffer paramEmail = new StringBuffer();
			paramEmail.append("correos="+detalleVO.getEmail());
			paramEmail.append("&json=").append(jsonObject);
			String respuesta = peticionUrlPostParams( URL_MAIL, paramEmail.toString());
			if(respuesta.equals("OK"))
				res = true;
		}catch(Exception e){
			logger.error("Error al enviar el correo: " + e.getMessage());
		}
		return res;
	}

	private static String peticionUrlPostParams(String urlRemota,String parametros) throws IOException {
		String respuesta = "Sin respuesta";
		URL url = new URL(urlRemota);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		// Se indica que se escribira en la conexión
		con.setDoOutput(true);
		con.setRequestMethod("POST");
		// Se escribe los parametros enviados a la url
		OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
		logger.info("antes de escribir parametros en la petición");
		wr.write(parametros);
		wr.close();			
		if(con.getResponseCode() == HttpURLConnection.HTTP_OK ||
				con.getResponseCode() == HttpURLConnection.HTTP_ACCEPTED){
			logger.info("Respuesta correcta ");
			respuesta = "OK";
		}else{
			respuesta = "ERROR";
		}
		con.disconnect();
		
		return respuesta;
	}

}
