package com.addcel.tlajomulco.controller;

import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.tlajomulco.model.vo.pagos.ProcomVO;
import com.addcel.tlajomulco.model.vo.pagos.TransactionProcomVO;
import com.addcel.tlajomulco.model.vo.response.ResponseProcesaProsa;
import com.addcel.tlajomulco.services.TlajomulcoPagoService;
import com.addcel.tlajomulco.services.TlajomulcoService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class TlajomulcoController {
	
	private static final Logger logger = LoggerFactory.getLogger(TlajomulcoController.class);
	@Autowired
	private TlajomulcoService tService;
	@Autowired
	private TlajomulcoPagoService tpService;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);				
		return "home";
	}
	
	@RequestMapping(value = "/addUser",method=RequestMethod.POST)
	public @ResponseBody String addUser(@RequestParam("json")String json) {
		logger.info("Dentro del servicio: /addUser");
		return tService.addUser(json);
	}
	
	@RequestMapping(value = "/login",method=RequestMethod.POST)
	public @ResponseBody String login(@RequestParam("json")String json) {	
		logger.info("Dentro del servicio: /login");
		return tService.login(json);
	}
	
	@RequestMapping(value = "/updateUser",method=RequestMethod.POST)
	public @ResponseBody String updateUser(@RequestParam("json")String json) {
		logger.info("Dentro del servicio: /updateUser");
		return tService.updateUser(json);
	}
	
	@RequestMapping(value = "/getTiendas",method=RequestMethod.POST)
	public @ResponseBody String listaTiendas(@RequestParam("json") String data) {	
		logger.info("Dentro del servicio: /getTiendas");
		return tService.listaTiendas(data);
	}
	
	@RequestMapping(value = "/findUser",method=RequestMethod.POST)
	public @ResponseBody String findUser(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: /findUser");
		return tService.findUser(data);
	}
	
	@RequestMapping(value = "/getRoles",method=RequestMethod.POST)
	public @ResponseBody String listaRoles(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: /getRoles");
		return tService.listaRoles(data);
	}
	
	@RequestMapping(value = "/getOperador",method=RequestMethod.POST)
	public @ResponseBody String getVendor(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: /getVendedor");
		return tService.getVendor(data);
	}
	
	@RequestMapping(value = "/getToken")
	public @ResponseBody String getToken() {		
		logger.info("Dentro del servicio: /getToken");
		return tService.generaToken();
	
	}
			
	@RequestMapping(value = "/deleteUser",method=RequestMethod.POST)
	public @ResponseBody String deleteUser(@RequestParam("json") String jsonEnc) {	
		logger.info("Dentro del servicio: /deleteUser");
		return tService.deleteUser(jsonEnc);	
	}
	
	/**
	 * Se utiliza para los roles supervisor y vendedor
	 * @param jsonEnc
	 * @return string encriptado con la respuesta del servicio
	 */
	@RequestMapping(value = "/updatePass",method=RequestMethod.POST)
	public @ResponseBody String actualizarPassword(@RequestParam("json") String jsonEnc) {	
		logger.info("Dentro del servicio: /updatePass");
		return tService.actualizaPassword(jsonEnc,0);
	}
	
	@RequestMapping(value = "/updatePassStatus",method=RequestMethod.POST)
	public @ResponseBody String actualizarPasswordStatus(@RequestParam("json") String jsonEnc) {	
		logger.info("Dentro del servicio: /updatePassStatus");
		return tService.actualizaPassword(jsonEnc,1);
	}
	@RequestMapping(value = "/consultaAdeudos", method=RequestMethod.POST)
	public @ResponseBody String consultaAdeudos(@RequestParam("json") String data){
		logger.info("Dentro del servicio: /consultaAdeudos");
		return tService.getAdeudos(data);
	}
	@RequestMapping(value = "/pago-ventanilla")
	public ModelAndView datosPago(@RequestParam("json") String jsonEnc) {	
		logger.info("Dentro del servicio: /pago-ventanilla");
		ModelAndView mav=new ModelAndView("error");
		ProcomVO procom = tpService.procesaPago(jsonEnc);
		if(procom!=null){			
			if(procom.getIdError()==0)
				mav=new ModelAndView("comerciofin");
			mav.addObject("prosa", procom);
			mav.addObject("processId", "1");
		}
		return mav;	
	}
	
	@RequestMapping(value = "/comercio-fin")
	public String comercioFin(Model model) {
		logger.info("Dentro del servicio: /comercio-fin");
		return "comerciofin";	
	}
	
	@RequestMapping(value = "/pagina-prosa")
	public String paginaTestProsa(Model model) {
		logger.info("Dentro del servicio: /pagina-prosa");
		//dfpService.comercioFin(user, referencia, monto, idTramite)
		//model.addAttribute("prosa", arg1);
		return "pagina-prosa";	
	}
	@RequestMapping(value = "/download")
	public String download(Model model) {
		logger.info("Dentro del servicio: /download");
		//dfpService.comercioFin(user, referencia, monto, idTramite)
		//model.addAttribute("prosa", arg1);
		return "download";	
	}
	
	@RequestMapping(value = "/comercio-con",method=RequestMethod.POST)
	public ModelAndView respuestaProsa(HttpServletRequest request) {
		String EM_Response = null, EM_Total = null, EM_OrderID = null, 
			   EM_Merchant = null, EM_Store = null, EM_Term = null,
			   EM_RefNum = null, EM_Auth = null, EM_Digest = null, 
			   email = null, cc_numberback = null, cc_nameback = null;

		logger.info("Dentro del servicio: /comercio-con");
		ModelAndView mav=new ModelAndView("error");
		ResponseProcesaProsa respuesta = null;
						
		@SuppressWarnings("unchecked")
		Map<String, String[]> parameters = request.getParameterMap();

	    for(String key : parameters.keySet()) {
	        logger.info(key);
	        String[] vals = parameters.get(key);
	        if(key.equals("EM_Response"))
	        	EM_Response = vals[0];
	        else if(key.equals("EM_Total"))
	        	EM_Total = vals[0];
	        else if(key.equals("EM_OrderID"))
	        	EM_OrderID = vals[0];
	        else if(key.equals("EM_Merchant"))
	        	EM_Merchant = vals[0];
	        else if(key.equals("EM_Store"))
	        	EM_Store = vals[0];
	        else if(key.equals("EM_Term"))
	        	EM_Term = vals[0];
	        else if(key.equals("EM_RefNum"))
	        	EM_RefNum = vals[0];
	        else if(key.equals("EM_Auth"))
	        	EM_Auth = vals[0];
	        else if(key.equals("EM_Digest"))
	        	EM_Digest = vals[0];
	        else if(key.equals("email"))
	        	email = vals[0];
	        else if(key.equals("cc_number"))
	        	cc_numberback = vals[0];
	        else if(key.equals("cc_name"))
	        	cc_nameback = vals[0];
	        
	        for(String val : vals)
	        	logger.info(" -> " + val);
	    }
		
		TransactionProcomVO tp= new TransactionProcomVO();
		tp.setEmAuth(EM_Auth);
		tp.setEmMerchant(EM_Merchant);
		tp.setEmOrderID(EM_OrderID);
		tp.setEmRefNum(EM_RefNum);
		tp.setEmResponse(EM_Response);
		tp.setEmStore(EM_Store);
		tp.setEmTerm(EM_Term);
		tp.setEmTotal(EM_Total);
		tp.setEmDigest(EM_Digest);
		logger.debug("email: {}",email);		
		
		respuesta = tpService.procesaRespuestaProsa(tp,email,cc_numberback,cc_nameback);
		if(respuesta != null ){			
			mav = new ModelAndView(respuesta.getRespuesta());
			tp.setComision(respuesta.getComision());
			tp.setCuenta(respuesta.getCuenta());
			tp.setTipoServicio(respuesta.getTipoServicio());
			tp.setEmTotal(EM_Total.substring(0,EM_Total.length()-2) + "." + EM_Total.substring(EM_Total.length()-2));
			mav.addObject("prosa", tp);
			mav.addObject("processId", "2");
		}
		return mav;	
	}
	
	@RequestMapping(value = "/busquedaPagos",method=RequestMethod.POST) 
	public @ResponseBody String busquedaPagos(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: /busquedaPagos");
		return tService.busquedaPagos(data);
	}
	
	@RequestMapping(value = "/reenvioRec",method=RequestMethod.POST) 
	public @ResponseBody String reenvioRec(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: /reenvioRec");
		return tService.reenvioRecibo(data);
	}	
	
	@RequestMapping(value = "/getListServicios")
	public @ResponseBody String getListServicios() {		
		logger.info("Dentro del servicio: /getListServicios");
		return tService.getListaServicio();	
	}
	@RequestMapping(value = "/updateServicio",method=RequestMethod.POST) 
	public @ResponseBody String updateServicio(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: /updateServicio");
		return tService.updateServicioStatus(data);
	}

}