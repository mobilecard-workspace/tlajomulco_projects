package com.addcel.tlajomulco.services;

import java.io.BufferedReader;


import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.tlajomulco.model.vo.pagos.TBitacoraProsaVO;
import com.addcel.tlajomulco.model.mapper.BitacorasMapper;
import com.addcel.tlajomulco.model.mapper.TlajomulcoMapper;
import com.addcel.tlajomulco.model.vo.ComisionVO;
import com.addcel.tlajomulco.model.vo.TlajomulcoDetalleVO;
import com.addcel.tlajomulco.model.vo.pagos.ProcomVO;
import com.addcel.tlajomulco.model.vo.pagos.ProsaPagoVO;
import com.addcel.tlajomulco.model.vo.pagos.TBitacoraVO;
import com.addcel.tlajomulco.model.vo.pagos.TransactionProcomVO;
import com.addcel.tlajomulco.model.vo.request.DatosPago;
import com.addcel.tlajomulco.model.vo.response.DetalleVO;
import com.addcel.tlajomulco.model.vo.response.ResponseProcesaProsa;
import com.addcel.tlajomulco.utils.AddCelGenericMail;
import com.addcel.tlajomulco.utils.UtilsService;
import com.addcel.utils.AddcelCrypto;

@Service
public class TlajomulcoPagoService {
	private static final Logger logger = LoggerFactory.getLogger(TlajomulcoPagoService.class);
	
//	private static final String URL_AUT_PROSA = "http://localhost:8080/ProsaWeb/ProsaAuth?";
	private static final String URL_AUT_PROSA = "http://50.57.192.213:8080/ProsaWeb/ProsaAuth";
//	private static final String URL_TIPO_CAMBIO="http://localhost:8080/Conversor/currency?fromCurrency=usd&toCurrency=MXN";
	@Autowired 
	private TlajomulcoMapper mapper;
	@Autowired
	private UtilsService utilService;	
	@Autowired
	private BitacorasMapper bm;
	@Autowired
	private TlajomulcoMapper mapperT;
	
	private static final String patron = "ddhhmmss";
	private static final String patronCom = "yyyy-MM-dd hh:mm:ss";
	private static final SimpleDateFormat formatoCom = new SimpleDateFormat(patronCom);
	private static final SimpleDateFormat formato = new SimpleDateFormat(patron);
	
	public ProcomVO procesaPago(String jsonEnc){		
		// prueba
		DatosPago datosPago = null;
		ProcomVO procom = null;
				
		datosPago = (DatosPago) utilService.jsonToObject(AddcelCrypto.decryptSensitive(jsonEnc), DatosPago.class);
		
		
		if(datosPago.getToken() == null){
			procom = new ProcomVO(1,"El parametro TOKEN no puede ser NULL");
			logger.error("El parametro TOKEN no puede ser NULL");
		}else{
			datosPago.setToken(AddcelCrypto.decryptHard(datosPago.getToken()));
			logger.info("token ==> {}",datosPago.getToken());
			
			if((mapperT.difFechaMin(datosPago.getToken())) > 3000){
//			if(false){
				procom = new ProcomVO(2,"La Transacción ya no es válida");
				logger.info("La Transacción ya no es válida");
			}else{
				TlajomulcoDetalleVO td = (TlajomulcoDetalleVO)utilService.jsonToObject(AddcelCrypto.decryptSensitive(jsonEnc), TlajomulcoDetalleVO.class);
				Integer i = bm.validaAddFDetalle(td);
				if(i.intValue()>0){
					procom = new ProcomVO(1,"El pago ya fue realizado");
				}else{
					String cuenta_id = mapperT.getCuentaID(td);
					if(cuenta_id!=null)
						td.setCuenta_id(cuenta_id);
					long idBitacora=insertaBitacoras(datosPago, td);
					procom = comercioFin(String.valueOf(datosPago.getId_usuario()), String.valueOf(idBitacora), Double.toString(datosPago.getTotal_pago().doubleValue()), "0",datosPago.getEmail());
				}
			}
		}
		
		return procom;
	}	
	
	private long insertaBitacoras(DatosPago dp, TlajomulcoDetalleVO td){
		TBitacoraVO tb = new TBitacoraVO();
		tb.setIdUsuario(dp.getId_usuario());
		tb.setBitConcepto("PAGO TLAJOMULCO 3DSECURE");
		tb.setImei(dp.getImei());
		switch(dp.getTipo_servicio().intValue()){
			case 1:
				tb.setDestino("Pago Predial: " + dp.getCuenta());
				break;				
			case 2:	
				tb.setDestino("Pago Agua: " + dp.getCuenta());
				break;
		}
		tb.setBitCargo(dp.getTotal_pago().toString());
		tb.setTipo(dp.getTipo());
		tb.setSoftware(dp.getSoftware());
		tb.setModelo(dp.getModelo());
		tb.setWkey(dp.getWkey());
		
		bm.addBitacora(tb);
		
		TBitacoraProsaVO tbProsa = new TBitacoraProsaVO();
		tbProsa.setIdBitacora(tb.getIdBitacora());
		tbProsa.setIdUsuario(Long.parseLong(tb.getIdUsuario()));
		tbProsa.setConcepto("PAGO TLAJOMULCO 3DSECURE");
		tbProsa.setCargo(dp.getTotal_pago().doubleValue());
		tbProsa.setCx(dp.getCx());
		tbProsa.setCy(dp.getCy());
				
		bm.addBitacoraProsa(tbProsa);
		td.setId_bitacora(tb.getIdBitacora());
		
		bm.addTDetalle(td);		
		
		return tb.getIdBitacora();
	}
			
	private ProcomVO comercioFin(String user,String referencia,String monto,String idTramite,String email) {    	        
        String varTotal= formatoMontoProsa(monto);
        String varMerchant = obtenParametro("@TlajomulcoVarMerchant");
        String varOrderId = referencia;
    	String varStore =obtenParametro("@TlajomulcoVarStore");
    	String varTerm = obtenParametro("@TlajomulcoVarTerm");
    	String varCurrency = obtenParametro("@TlajomulcoVarCurrency");
    	String varAddress = obtenParametro("@TlajomulcoVarAddress");    

    	logger.info("varMerchant: " + varMerchant +
    				"\nvarOrderid: " + varOrderId +
    				"\nvarStore: " + varStore + 
    				"\nvarTerm: " + varTerm +
    				"\nvarCurrency: " + varCurrency +
    				"\nvarAddress: " + varAddress);
    	
        //cambiar por datos correctos
        String digest = digestEnvioProsa(varMerchant, varStore, varTerm, varTotal, varCurrency, referencia);   
        logger.info("Digest Tlajomulco: {}",digest);
        							        
    	ProcomVO procomObj=new ProcomVO(varTotal, varCurrency, varAddress, referencia, varMerchant, varStore, varTerm, digest, "", user, idTramite,email);
    	
    	logger.info("User Tlajomulco: {}",user);
    	logger.info("Referencia Tlajomulco: {}",referencia);
    	logger.info("IdTramite Tlajomulco: {}",idTramite);
    	
    	return procomObj;
    }
	
	private String formatoMontoProsa(String monto){
		String varTotal = "000";
		String pesos = null;
        String centavos = null;
		if(monto.contains(".")){
            pesos=monto.substring(0, monto.indexOf("."));
            centavos=monto.substring(monto.indexOf(".")+1,monto.length());
            if(centavos.length()<2){
                centavos = centavos.concat("0");
            }else{
                centavos=centavos.substring(0, 2);
            }            
            varTotal=pesos+centavos;
        }else{
            varTotal=monto.concat("00");
        } 
		logger.info("Monto a cobrar 3dSecure: "+varTotal);
		return varTotal;		
	}
	
	private String digest(String text){
		logger.info("Cadena antes de digest: "+ text);
		String digest="";
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			md.update(text.getBytes(),0,text.length());
			byte[] sha1=md.digest();
			digest = convertToHex(sha1);
		} catch (NoSuchAlgorithmException e) {
			logger.info("Error al encriptar - digest", e);
		}
		return digest;
	}
	
	private String convertToHex(byte[] data) { 
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < data.length; i++) { 
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do { 
                if ((0 <= halfbyte) && (halfbyte <= 9)) 
                    buf.append((char) ('0' + halfbyte));    
                else 
                    buf.append((char) ('a' + (halfbyte - 10)));
                halfbyte = data[i] & 0x0F;
            } while(two_halfs++ < 1);
        } 
        return buf.toString();
    } 
	
	public String digestEnvioProsa(String varMerchant,String varStore,String varTerm,String varTotal,String varCurrency,String varOrderId){
		return digest(varMerchant+varStore+varTerm+varTotal+varCurrency+varOrderId);		
	}
	
	public String digestRegresoProsa(TransactionProcomVO transactionProcomV) {
		StringBuffer digestCad = new StringBuffer()
				.append(transactionProcomV.getEmTotal())
				.append(transactionProcomV.getEmOrderID())
				.append(transactionProcomV.getEmMerchant())
				.append(transactionProcomV.getEmStore())
				.append(transactionProcomV.getEmTerm())
				.append(transactionProcomV.getEmRefNum())
				.append("-")
				.append(transactionProcomV.getEmAuth());
				
		return digest(digestCad.toString());		
	}			
	
	public ResponseProcesaProsa procesaRespuestaProsa(TransactionProcomVO tp,String email, String last4d, String nombrecard) {
		// insertar bitacora ecommerce
		// Valida digest,
		logger.info("last4d:" + last4d + ", nombrecard: " + nombrecard);
		List<DetalleVO> detalleVO = null;
		String paginaRespuesta = "error";
		ResponseProcesaProsa respuesta = new ResponseProcesaProsa();
		String digest = digestRegresoProsa(tp);
		TBitacoraVO b = new TBitacoraVO();
		TBitacoraProsaVO tbp = new TBitacoraProsaVO();
		
		/*
		String patron = "000000";
    	String patron2 = "000000000000";
    	DecimalFormat formato = new DecimalFormat(patron);
    	DecimalFormat formato2 = new DecimalFormat(patron2);
    	
    	Random  rnd = new Random();
    	
    	int aut = (int)(rnd.nextDouble() * 900000);
    	long ref = (long)(rnd.nextDouble() * 900000000);
    	
    	//tp.setEmAuth(formato.format(aut));
    	//tp.setEmRefNum(formato2.format(ref));

    	 */
		if (tp.getEmDigest() != null && tp.getEmDigest().equals(digest)) {
			if (!tp.getEmAuth().equals("000000")) {							
				b.setIdBitacora(Long.parseLong(tp.getEmOrderID()));
				b.setBitStatus(1);
				b.setBitConcepto("PAGO TLAJOMULCO 3DSECURE EXITOSO");
				b.setBitNoAutorizacion(tp.getEmAuth());
				b.setTarjetaCompra("XXXX-XXXX-XXXX-"+last4d);
				bm.updateBitacora(b);
				tbp.setIdBitacora(Long.parseLong(tp.getEmOrderID()));
				tbp.setAutorizacion(tp.getEmAuth());
				bm.updateBitacoraProsa(tbp);
				// idStatus
				bm.updateTDetalle(Integer.parseInt(tp.getEmOrderID()), 1,nombrecard);
				// envio correo
				
				paginaRespuesta = "comerciocon";
				logger.info("Pago correcto");
				
				detalleVO = mapperT.getDetalle(null, null, null, null, null, null, tp.getEmOrderID());				
				if(detalleVO != null && detalleVO.size() >0){
					respuesta.setCuenta(detalleVO.get(0).getCuenta());
					respuesta.setComision(detalleVO.get(0).getComision().toString());
					respuesta.setTipoServicio(detalleVO.get(0).getTipo_servicio()==1?"Predial":"Agua");
					logger.info("Enviando correo a " + detalleVO.get(0).getEmail());
					if(detalleVO.get(0).getEmail()!=null)					
						AddCelGenericMail.generatedMail(detalleVO.get(0), utilService);
				}
			} else {					
				b.setIdBitacora(Long.parseLong(tp.getEmOrderID()));
				b.setBitStatus(0);
				b.setBitConcepto("PAGO DUTTY FREE 3DSECURE NO AUTORIZADA");
				b.setBitNoAutorizacion(tp.getEmAuth());
				bm.updateBitacora(b);
				// idStatus
				bm.updateTDetalle(Integer.parseInt(tp.getEmOrderID()), 0, nombrecard);
				paginaRespuesta = "error";
				logger.error("Transacción no autorizada: {}", tp.getEmAuth());
			}
		} else {					
				b.setIdBitacora(Long.parseLong(tp.getEmOrderID()));
				b.setBitStatus(0);
				b.setBitConcepto("PAGO DUTTY FREE 3DSECURE NO AUTORIZADA");
				b.setBitNoAutorizacion(tp.getEmAuth());
				bm.updateBitacora(b);
				// idStatus
				bm.updateTDetalle(Integer.parseInt(tp.getEmOrderID()), 0, nombrecard);
				paginaRespuesta = "error";
				logger.error("Transacción no autorizada: {}", tp.getEmAuth());
		}
		respuesta.setRespuesta(paginaRespuesta);
		return respuesta;
	}	
	private String obtenParametro(String reference){
		String param = null;/*
		refreshParam(reference);			
		param = (String)lookupReference(reference);
		if(param == null){*/
			param = mapperT.getParametro(reference);/*
			insertReference(reference, param);
		}*/			
		return param;
	}
	private Object lookupReference(String jndi)
    {
		logger.info("inicio lookupReference:");
        InitialContext contexto;
        Object obj;
        try {
        	logger.info("Buscando en el contexto :"+jndi);
            contexto = new InitialContext();
            obj = contexto.lookup(jndi);
            logger.info(jndi+" Encontrado, del tipo "+obj.getClass().getName());
        } catch (NamingException e) {
        	logger.info(jndi+ " no esta en el contexto");                       
            obj = null;
        }       
        logger.info("Fin lookupReference:");
        return obj;
    }
    private void removeReference(String jndi) 
    {
    	logger.info("inicio removeReference:");
        InitialContext contexto;
        try {
            contexto = new InitialContext();
            contexto.unbind(jndi);
            contexto.removeFromEnvironment(jndi);
        } catch (NamingException e) {
        	logger.info("Fin removeReference, error: " + e.getMessage());   
        }
        logger.info("Fin removeReference:");       
    }
 
    private void insertReference(String jndi, Object obj)
    {
    	logger.info("inicio insertReference:");
    	try{
    		InitialContext contexto;
    		contexto = new InitialContext();
    		if(lookupReference(jndi)==null){
    			logger.info("insertando en el contexto: "+ jndi);
    			contexto.addToEnvironment(jndi,obj);
    			contexto.bind(jndi,obj);
    		} else
    		{
    			logger.info("actualizando en el contexto : "+ jndi);
    			contexto.rebind(jndi,obj);
    		}
    		logger.info("fin insertReference:");
    	} catch(Exception e){
    		logger.error("fin insertReference, no se pudo insertar en el contexto : " +e.getMessage());
    	}
    }
    private void refreshParam(String reference){
    	
    	try{
    		SimpleDateFormat formato = new SimpleDateFormat("ddMMyyyy");
    		Calendar cal =  GregorianCalendar.getInstance();
    		Date fecha = cal.getTime();
    		String hoy = formato.format(fecha);
    		String fechaVigencia = (String)lookupReference(reference + "FechaVigencia");
    		logger.info("Hoy:" + hoy);
    		logger.info("Fecha vigencia parametros contexto:" + fechaVigencia);
    		if(fechaVigencia!=null){
    			if(!hoy.equals(fechaVigencia)){
    				removeReference(reference);
    				insertReference(reference + "FechaVigencia", hoy);
    			}
    		}else
    			insertReference(reference +"fechaVigencia", hoy);
    	}catch(Exception e){
    		logger.error("Error al refrescar el parametro " + reference + " : "+ e.getMessage());
    	}
    }
}
